# Shopify APP - Scaffold-metafield  

### Installation

Install the dependencies and devDependencies.

**Run below command in terminal on your project folder like** _/var/www/html/project-folder_
```sh
$ cd  /var/www/html/project-folder
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, shopify api key and secret, mail credentials)
$ php artisan key:generate
$ php artisan queue:table
$ php artisan migrate
```

**(1) Edit config in .env**

    QUEUE_CONNECTION=database       

    $ php artisan queue:table


**(2) Edit Database Details in .env file**
  
     DB_CONNECTION=mysql
     DB_HOST=127.0.0.1
     DB_PORT=3306
     DB_DATABASE=laravel
     DB_USERNAME=root
     DB_PASSWORD=


**(3) Edit Shopify APP Credential Here in .env file**
  
    SHOPIFY_API_KEY=
    SHOPIFY_API_SECRET=


**(4) For development environments...**

```sh
$ npm install
$ npm run dev
```
   **For production environments...**

```sh
$ npm install --production
$ npm run prod
```
 **(5) Setup superviser for Queue JOB in Server**
     


   //------supervisor------------//

      Note* For php artisan work:queue => it's genrate cache
            Neet to restart job file if any chages if set work cmd because code call from cache and updated code not call.         
            php artisan listen => every time code exceute.
  

 **Steps** 

* $ cd /etc/supervisor/conf.d/ 
* $ touch scaffold-metafield.conf
* $ nano scaffold-metafield.conf
    
* Add below code in  scaffold-metafield.conf
 
* Replace project-name to scaffold-metafield in code

*  $ ctr+X.  save=>y
  
* $ sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl restart project-name 
 
* **scaffold-metafield.conf** file code


        [program:project-name]
        #process_name=%(program_name)s_%(process_num)02d
        command=php /var/www/html/project-folder/artisan queue:work --daemon --sleep=3 --tries=1 --timeout=0
        #command=php /var/www/html/project-folder/artisan cache:clear
        autostart=true
        autorestart=true
        #user=www-data
        #numprocs=8
        #redirect_stderr=true
        #stderr_logfile=/var/www/html/project-folder/queue-worker-err.log
        #stdout_logfile=/var/www/html/project-folder/queue-worker-out.log

* This app supervisor commands will be


```sh
$ sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl restart scaffold-metafield
```

  **(6) Setup CRON job in server**
  
  **$ crontab -e**
  
  ```cd /var/www/html/project-folder && php artisan schedule:run >> /dev/null 2>&1``` 



  **(7) Run This commands in project root for Storage permisstion.**
  
      $ sudo chown -R www-data:www-data storage
      $ sudo chmod -R 775 storage
      $ sudo chmod -R 775 bootstrap/cache
        
   **(8) Use This Command for clear cache App**
   
     $  php artisan optimize:clear     


  **(9) Clearing Configuration Cache**
  
   _Whenever edit or change .env file Run below command in project root_
  
  ```php artisan config:clear``` 


##### ***Note* : ****/var/www/html/project-folder****            
 * This Url is your server project root path
