<?php


if(!function_exists('field_type_options')){

     function field_type_options(){

         try {

             $data = [
                 "text" => "Text",
                 "wysiwyg" => "Wysiwyg",
                 "image" => "Image",
                 "select" => "Select",
                 "collection" => "Collection"
             ];

             return $data;

         }catch (\Exception $e){
             logger('=========== ERROR :: Helper :: FieldTypeOption ===========');
             logger(json_encode($e));
         }

     }

}


