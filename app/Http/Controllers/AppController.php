<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Response;

class AppController extends Controller
{


    public function dashboard(){
        return view('dashboard');
    }


    public function fieldTypeOption(Request $request){

        try{

           // $data = field_type_options();


            $data = [
                ['id' => 1, 'name' => 'Ford'],
                ['id' => 2, 'name' => 'BMW'],
                ['id' => 3, 'name' => 'Audi']
            ];


            return  Response::json(['success' => true,"data" => $data,'message' => 'Field Type Options Listed successfully.'], 200);

        }catch(Exception $e){

            logger("============================Error :: FieldTypeOption================================");
            logger(json_encode($e->getMessage()));

            return  Response::json(['success' => false,'message' => $e->getMessage()]);
        }

    }

}
