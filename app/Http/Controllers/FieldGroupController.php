<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use Response;
use App\Models\FieldGroup;
use App\Models\FieldGroupItems;
use App\Http\Requests\FeildGroup;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Nette\Utils\Json;

class FieldGroupController extends Controller
{

  //index
   public function index(Request $request)
   {
       try{
         //  $shop = Auth::user();
           $user_id = 1;
           logger("=====shop=====");
           //logger(json_encode($shop));
           $data = FieldGroup::with('FieldGroupItems')->where('user_id',$user_id)->orderBy('created_at', 'desc')
               ->get();
           return  Response::json(['success' => true,"data" => $data,'message' => 'Groups Listed successfully.'], 200);

       }catch(Exception $e){
           logger("============================Error :: fieldGroups================================");
           logger(json_encode($e->getMessage()));
       }

   }

   //store
   public function store(FeildGroup $request){
       try{
           $shop = Auth::user();
           $user_id = 1;
           logger(json_encode($request->all()));

           $input = $request->all();

           $field_group = $input['field_group_items'];

           $group_row = [
               'user_id' => $user_id,
               'name' => $input['name'] 
           ];
           $group = FieldGroup::create($group_row);

           if(count($field_group)>0){

               $items = [];

               for($i=0; $i<count($field_group); $i++){

                   $items[] = [
                       'field_label' => $field_group[$i]['field_label'],
                       'field_name'  => $field_group[$i]['field_name'],
                       'field_type'  => $field_group[$i]['field_type'],
                   ];
               }
            //  FieldGroupItems::insert($items);
               $item = new FieldGroupItems();
               $id = $group->id;
               $item->field_group_db_id = $id;
               $item->value = json_encode($items);
               $item->save();
           }
           return  Response::json(['success' => true,"data" => [],'message' => 'Groups added successfully.'], 200);


       }catch(Exception $e){
           logger("============================Error :: fieldGroups================================");
           logger(json_encode($e->getMessage()));
       }
   }

//edit
    public function edit(Request $request,$id){
        try{
            $shop = Auth::user();
            $data = FieldGroup::with('FieldGroupItems')->find($id);
            $array = [];
            foreach($data->FieldGroupItems as $field){
                // $field
                $array[] = json_decode($field->value);
            }
            $datas = $array;
            return  response()->json(['success' => true,"data" => $datas,'message' => 'Groups Edit listed.'], 200);
        }catch(Exception $e){
            logger("============================Error :: fieldGroups================================");
            logger(json_encode($e->getMessage()));
        }
    }


    public function update(Request $request,$id){
        try{
            logger(json_encode($request->all()));
            $arr = [];
            $item = FieldGroupItems::where('field_group_db_id',$id)->first();
            foreach($request->fieldsValues as $fieldValue){
                $arr[] = [
                    'field_name' => $fieldValue['field_name'],
                    'field_label' => $fieldValue['field_label'],
                    'field_type' => $fieldValue['field_type'],
                ];
            } 
            $item->value = $arr;
            $item->save();
            return  Response::json(['success' => true,"data" => [],'message' => 'Field group updated successfully.'], 200);

        }catch(Exception $e){
            return response()->json([
                "success" => false,
                "message" => $e->getMessage()
            ],422);
            logger("============================Error :: fieldGroups================================");
            logger(json_encode($e->getMessage()));
        }
    }

    public function destroy(Request $request,$id){
        try{

            $shop = Auth::user();

            logger(json_encode($request->all()));

            $input = $request->all();

            FieldGroup::where('id',$id)->delete();

            return  Response::json(['success' => true,"data" => [],'message' => 'Field group Deleted successfully.'], 200);


        }catch(Exception $e){
            logger("============================Error :: fieldGroups================================");
            logger(json_encode($e->getMessage()));
        }
    }
}
