<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FieldGroupItems;
use App\Models\FieldGroup;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FieldGroupItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            //$shop = Auth::user();
              $user_id = 1;
              logger("=====shop=====");
              //logger(json_encode($shop));
              $fid = FieldGroupItems::first();
              $data = FieldGroup::with('FieldGroupItems')->where('field_group_db_id',$fid->field_group_db_id)->orderBy('created_at', 'desc')
                ->first();
              return Response::json(['success' => true,"data" => $data,'message' => 'Item Groups Listed successfully.'], 200);
          }catch(Exception $e){
              logger("============================Error :: fieldGroups================================");
              logger(json_encode($e->getMessage()));
          }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
