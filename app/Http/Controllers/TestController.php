<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Exception;
use Response;
class TestController extends Controller
{

      public function index(Request $request){
          return "test";
      }

      public function sendReq(Request $request){


      }

      public function productList(Request $request){

          try{

          logger("========START :: Test/API :: products=========");

          $response = Http::get('https://hairbawse.com/integration/shopify/products');

          $data = $response->json();

          return  Response::json(['success' => true,"data" => $data,'message' => 'Products Retrived successfully.'], 200);

          }catch(Exception $e){
             logger("========Error :: Test/API :: products=========");
             logger(json_encode($e));

             return [ 'success' => false, "data" => [], 'message' => $e->getMessage()];
          }


      }

}
