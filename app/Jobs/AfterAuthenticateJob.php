<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ShopifyShop;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $shopAuthUser;

    public function __construct($shopAuthUser)
    {
        logger("-----------------------------------");
        logger("AfterAuthenticateJob Job is start!");
        logger("-----------------------------------");
        logger("shopAuthUser objct:".json_encode($shopAuthUser));
        $this->shopAuthUser = $shopAuthUser;
        logger("name :".$shopAuthUser['name']);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger("AfterAuthenticateJob Job is handle!");
            $shop = User::where('name', $this->shopAuthUser['name'])->firstOrFail();

            if ($shop) {

                logger("Shop's API objct:".json_encode($shop));

            $user_id = $shop['id'];
            $shopApi = $shop->api()->rest('GET', '/admin/shop.json')['body']['shop'];
            if ($shopApi) {
                logger("Shop's API objct:".json_encode($shopApi));
                ShopifyShop::updateOrCreate(
                    ["shop_id" => $shopApi['id']],
                    [
                        "shop_id" => $shopApi['id'],
                        "name" => $shopApi['name'],
                        "email" => $shopApi['email'],
                        "domain" => $shopApi['domain'],
                        "province" => $shopApi['province'],
                        "country" => $shopApi['country'],
                        "address1" => $shopApi['address1'],
                        "zip" => $shopApi['zip'],
                        "city" => $shopApi['city'],
                        "primary_locale" => $shopApi['primary_locale'],
                        "country_code" => $shopApi['country_code'],
                        "country_name" => $shopApi['country_name'],
                        "currency" => $shopApi['currency'],
                        "shop_owner" => $shopApi['shop_owner'],
                        "weight_unit" => $shopApi['weight_unit'],
                        "province_code" => $shopApi['province_code'],
                        "plan_name" => $shopApi['plan_name'],
                        "user_id" => $user_id
                    ]
                );

                logger("-----------------------------------");
                logger("Shop Data is Saved!");
                logger("-----------------------------------");

            }
            else {
                logger("error during call Shop API !");
            }

            } else {
                logger("-----------------------------------");
                logger("Shop Not Found!");
                logger("-----------------------------------");
            }


        }
        catch(\Exception $e){
            logger("AfterAuthenticateJob Job is Exception!");
            logger('Message: '.$e->getMessage());
        }

        logger("-----------------------------------");
        logger("AfterAuthenticateJob Job is end!");
        logger("-----------------------------------");
    }
}
