<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldGroup extends Model
{
    use HasFactory;

    protected $table = 'field_groups';

    protected $fillable = [
        'user_id',
        'name',
        'status',
    ];


    public function FieldGroupItems(){
        return $this->hasMany(FieldGroupItems::class, 'field_group_db_id', 'id' );
    }
}
