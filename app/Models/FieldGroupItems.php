<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldGroupItems extends Model
{
    use HasFactory;

    protected $table = 'field_group_items';

    protected $fillable = [
        'field_group_db_id',
        'field_label',
        'field_name',
        'field_type',
        'status',
    ];

}
