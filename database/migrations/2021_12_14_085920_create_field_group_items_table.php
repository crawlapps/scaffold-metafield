<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldGroupItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_group_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('field_group_db_id');
            $table->string('field_label')->nullable();
            $table->string('field_name')->nullable();
            $table->string('field_type')->nullable();
            $table->json('value')->nullable();
            $table->boolean('status')->default(1)->comment('0 = Deactiv, 1 = Active');
            $table->timestamps();
            $table->foreign('field_group_db_id')->references('id')->on('field_groups')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_group_items');
    }
}
