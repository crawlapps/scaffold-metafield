import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import enTranslations from '@shopify/polaris/locales/en.json';
import {AppProvider, Page, Card, Button} from '@shopify/polaris';
import { Provider,TitleBar } from '@shopify/app-bridge-react';
import createApp from '@shopify/app-bridge';


//Routes :: START
import WebRoutes from "./routes/index";
//Routes :: END

const config = window.shopify_app_bridge;

class App extends Component {
    render () {
        return (
                <div>
                    <WebRoutes />
                </div>
        )
    }
}

const root =  <AppProvider i18n={enTranslations}>
                    <App />
               </AppProvider>





ReactDOM.render(root, document.getElementById('app'))
