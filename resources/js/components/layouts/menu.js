import React from 'react'
import {Outlet, Link } from 'react-router-dom'

const Header = () => (
    <>
    <nav className='navbar navbar-expand-md navbar-light navbar-laravel'>
        <div className='container'>
             <ul>
                 {/* <li> <Link className='navbar-brand' to='/dashboard'>Test DEMO</Link></li>
                 <li> <Link className='navbar-brand' to='/'>Dashboard</Link></li>
                 <li> <Link className='navbar-brand' to='/product'>Product</Link></li>
                 <li> <Link className='navbar-brand' to='/home'>Home</Link></li>
                 <li> <Link className='navbar-brand' to='/contact'>Contact Me</Link></li> */}
             </ul>
        </div>
    </nav>

    <Outlet />
    </>
)

export default Header
