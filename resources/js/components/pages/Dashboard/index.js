import React, {Component} from 'react'
import ReactDOM from 'react-dom';



function Car(props) {z

    const shoot = () => {
        alert("Great Shot!");
    }

    return (
        <button onClick={shoot}>Take the shot! {props.brand}</button>
    );
}


class Index extends Component{

    constructor(props){
        super(props);
        this.state = {
             brand : "ford",
             model : "Mustang",
             color : "red",
             year  : 2022
        }
    }
    getSnapshotBeforeUpdate(prevProps, prevState) {
        document.getElementById("div1").innerHTML =
            "Before the update, the favorite was " + prevState.color;
    }
    componentDidUpdate() {
            document.getElementById("div2").innerHTML =
            "The updated favorite is " + this.state.color;
    }
    componentDidMount(){
        console.log("Method :: componentDidMount call");
        setTimeout(() => {
             this.setState({color:"Yellow"})
        },1000)
    }

    changeColor = () => {
        console.log("Method :: changeColor call");
        this.setState({
            color : "Blue"
        });
    }

    shouldComponentUpdate() {
        return true; //true or false
    }

    render(){
         return (
               <div>
                   <h1>My {this.state.brand}</h1>
                   <div id="div1"></div>
                   <div id="div2"></div>
                   <p>
                       It is a <b>{this.state.color} </b>
                         {this.state.model}
                          from   {this.state.year}.
                   </p>

                   <button type="button" onClick={this.changeColor}>Change color</button>
                    <Car brand="Ford56"/>
               </div>
         )
    }
}

export default Index
