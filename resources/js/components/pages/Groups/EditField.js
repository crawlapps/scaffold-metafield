import React, { Component, useState, useEffect } from 'react';
import ReactDom from 'react-dom';
import { Link, useParams, useNavigate } from "react-router-dom";
import axios from 'axios';
import Swal from 'sweetalert2';
import { Page, Card, Button, Caption, DataTable, Layout, Section, FormLayout, TextField, Select, InlineError } from '@shopify/polaris';
import List from './List';

function EditField() {

    const navigate = useNavigate();

    const { id } = useParams()

    const data = {
        name: '',
        fieldsValues: [{ field_label: "", field_name: "", field_type: "" }],
        formErrors: {}
    }
    const [validationError, setValidationError] = useState({})

    const [state, setState] = useState(data);

    const [group, setGroup] = useState("page");

    const [name, setName] = useState(data);

    const handleFieldChange = (i, event) => {

        let fieldsValues = state.fieldsValues;

        fieldsValues[i][event.target.name] = event.target.value;
        setState({ ...state, fieldsValues });
    }

    const handleFieldSelectChange = (i, event) => {

        let fieldsValues = state.fieldsValues;

        fieldsValues[i]['field_type'] = event;
        setState({ ...state, fieldsValues });
    }

    const handleChange = (event) => {

        // setState({
        //     [event.target.name] : event.target.value
        // });

        const { name, value } = event.target;
        setState({ ...state, [name]: value });

    }

    const addFormFields = () => {
        // setState(({
        //     fieldsValues: [...state.fieldsValues, { field_label: "", field_name : "", field_type : "" }]
        // }))

        setState({ ...state, fieldsValues: [...state.fieldsValues, { field_label: "", field_name: "", field_type: "" }] });
    }

    const removeFormFields = (i) => {

        let fieldsValues = state.fieldsValues;
        fieldsValues.splice(i, 1);
        setState({ ...state, fieldsValues });
    }
    useEffect(() => {
        fetchProduct()
    }, [])

    const fetchProduct = async () => {
        await axios.get(`/groups/edit/${id}`).then(({ data }) => {
            const { name } = data.data;
            setName(name);
            const field_group_items = data.data.field_group_items
            let group = {};
            
            field_group_items.map((item, i) => {
                group["field_label"] = item.field_label;
                group["field_name"] = item.field_name;
                group["field_type"] = item.field_type;ss
            });

            console.log("group", group);

            setState({ ...state, fieldsValues: [group] });


        }).catch(({ response: { data } }) => {
            Swal.fire({
                text: data.message,
                icon: "error"
            })
        })
    }


    const updateProduct = async (e, fieldsValues) => {
        e.preventDefault();
        // if (handleFormValidation()) {
        await axios({
            method: 'POST',
            url: `/groups/update/${id}`,
            data: { fieldsValues:fieldsValues } 
        })
            .then(({ data }) => {
                Swal.fire({
                    icon:"success",
                    text:data.message
                 })              
                navigate("/")
                .catch(({ response }) => {
                        if (response.status === 422) {
                            setValidationError(response.data.errors)
                        } else {
                            Swal.fire({
                                text: response.data.message,
                                icon: "error"
                            })
                        }
                    })
                })
        // }
    }

    const handleFormValidation = () => {
         axios.post(`/groups/update/${id}`, fieldsValues).then(({ data }) => {
            Swal.fire({
                icon: "success",
                text: data.message
            })
            navigate("/")
        }).catch(({ response }) => {
            if (response.status === 422) {
                setValidationError(response.data.errors)
            } else {
                Swal.fire({
                    text: response.data.message,
                    icon: "error"
                })
            }
        })
        // }

        const { name } = state;
        const { fieldsValues } = state.fieldsValues;

        let formfieldsErrors_msg = {};

        let formErrors = {};
        var formIsValid = true;

        //Student name
        if (!name) {
            formIsValid = false;
            formErrors["nameError"] = "Name is required.";
        }

        let fields_errors = state.fieldsValues.map((element, key) => {

            let formFieldErrors = {};

            if (!element.field_label) {
                formFieldErrors['field_label'] = "field label is required";
                formIsValid = false;
            }
            else {
                formFieldErrors['field_label'] = "";
            }

            if (!element.field_name) {
                formFieldErrors['field_name'] = "field name is required";
                formIsValid = false;
            }
            else {
                formFieldErrors['field_name'] = "";
            }

            if (!element.field_type) {
                formFieldErrors['field_type'] = "field type is required";
                formIsValid = false;
            }
            else {
                formFieldErrors['field_type'] = "";
            }

            return formFieldErrors;

        });

        formErrors['fieldsErrors'] = fields_errors;

        setState({ ...state, formErrors: formErrors });

        console.log("formIsValid", formIsValid);

        return formIsValid;

    }

    var fieldsErrors = [];

    console.log("state.formErrors", state.formErrors);

    if (state.formErrors && typeof (state.formErrors.fieldsErrors) !== 'undefined') {
        fieldsErrors = state.formErrors.fieldsErrors;
    }

    var { nameError } = '';

    if (state.formErrors && typeof (state.formErrors.nameError) !== 'undefined') {
        nameError = state.formErrors.nameError;
    }
     const options = [
        // {label: 'Field Type', value: ''},
        {label: 'Text', value: 'single_line_text_field'},
        {label: 'Wysiwyg', value: 'multi_line_text_field'},
        {label: 'Image', value: 'single_line_text_field'},
        {label: 'Select', value: 'multi_line_text_field'}        
    ];
    const options1 = [
        // {label: 'Field Type', value: ''},
        {label: 'Page', value: 'page'},
        {label: 'Products', value: 'products'},
        {label: 'Customers', value: 'customers'},
        {label: 'Blogs', value: 'blogs'},
        {label: 'Orders', value: 'orders'},
        {label: 'Product', value: 'product_reference'},
    ];
    const fieldsValues = state.fieldsValues;

    return (
        
        <div>
            <div className="Polaris-Layout">
                <div
                    className="Polaris-Layout__Section Polaris-Layout__Section--oneHalf">
                    <div className="Polaris-Card">
                        <div className="Polaris-Card__Header">
                            <div
                                className="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                <div
                                    className="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                    <h2 className="Polaris-Heading">Edit Field Group</h2>
                                </div>
                            </div>
                        </div>

                        <div className="Polaris-Card__Section">
                            <Layout>
                                <Layout.Section>
                                    <Card title="Order details" sectioned>
                                        <form onSubmit={updateProduct}>
                                            <FormLayout>
                                                <div>
                                                    <div className='Polaris-TextField'>
                                                        <input
                                                            id='name'
                                                            type='text'
                                                            className={`Polaris-TextField__Input ${typeof nameError !== 'undefined' ? 'error' : ''}`}
                                                            name='name'
                                                            value={name}
                                                            placeholder="Add Title"
                                                            onChange={handleChange}
                                                        />
                                                    </div>
                                                    {nameError &&
                                                        <InlineError message={nameError} fieldID="myFieldID" />
                                                    }
                                                </div>
                                                <List/>

                                                <div>
                                                    <div className="Polaris-Card full-border">
                                                        <div className="Polaris-Card__Header">
                                                            <h2 className="Polaris-Heading">Adding field</h2>
                                                        </div>
                                                        <div className="Polaris-Card__Section">

                                                            <div className="Polaris-Card">
                                                                <div className="Polaris-Card__Section">

                                                                    {state.fieldsValues && state.fieldsValues.map((element, index) => (

                                                                        <div className="Polaris-Card" key={index}>
                                                                            <div className="Polaris-Card__Section">
                                                                                <div>
                                                                                    <div className='Polaris-TextField'>
                                                                                        <input type="text"
                                                                                            name="field_label"
                                                                                            value={element.field_label || ""}
                                                                                            className="Polaris-TextField__Input"
                                                                                            placeholder="Field Label"
                                                                                            onChange={(event) => handleFieldChange(index, event)}
                                                                                        />
                                                                                    </div>

                                                                                    {typeof fieldsErrors !== 'undefined' && fieldsErrors.length > 0 && ((fieldsErrors.length - 1) >= index) && (
                                                                                        <InlineError message={fieldsErrors[index].field_label} fieldID={index} />
                                                                                    )}
                                                                                </div>


                                                                                <div>
                                                                                    <div className='Polaris-TextField'>
                                                                                        <input type="text"
                                                                                            name="field_name"
                                                                                            className="Polaris-TextField__Input"
                                                                                            value={element.field_name || ""}
                                                                                            placeholder="Field Name"
                                                                                            onChange={(event) => handleFieldChange(index, event)}
                                                                                        />
                                                                                    </div>
                                                                                    {typeof fieldsErrors !== 'undefined' && fieldsErrors.length > 0 && ((fieldsErrors.length - 1) >= index) && (
                                                                                        <InlineError message={fieldsErrors[index].field_name} fieldID={index} />
                                                                                    )}

                                                                                </div>

                                                                                <div>
                                                                                    <Select
                                                                                        placeholder="Field Type"
                                                                                        name="field_type"
                                                                                        options={options}
                                                                                        onChange={(event) => handleFieldSelectChange(index, event)}
                                                                                        value={element.field_type || ""}
                                                                                        multiple={true}

                                                                                    />

                                                                                    {typeof fieldsErrors !== 'undefined' && fieldsErrors.length > 0 && ((fieldsErrors.length - 1) >= index) && (
                                                                                        <InlineError message={fieldsErrors[index].field_type} fieldID={index} />
                                                                                    )}

                                                                                </div>
                                                                            </div>
                                                                            {
                                                                                index ?
                                                                                    <div
                                                                                        className="Polaris-Card__Footer">
                                                                                        <button onClick={() => removeFormFields(index)}
                                                                                            className="Polaris-Button Polaris-Button--destructive"
                                                                                            type="button"><span
                                                                                                className="Polaris-Button__Content"><span
                                                                                                    className="Polaris-Button__Text">Remove</span></span>
                                                                                        </button>
                                                                                    </div>
                                                                                    : null
                                                                            }
                                                                        </div>
                                                                    ))}

                                                                </div>
                                                                <div className="Polaris-Card__Footer">
                                                                    <div className="Polaris-ButtonGroup">
                                                                        <div className="Polaris-ButtonGroup__Item">
                                                                            <button className="Polaris-Button"
                                                                                type="button"><span
                                                                                    className="Polaris-Button__Content"><span
                                                                                        className="Polaris-Button__Text"
                                                                                        onClick={() => addFormFields()}
                                                                                    >Add Field</span></span>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="Polaris-Card__Footer">
                                                            <div className="Polaris-ButtonGroup">
                                                                <div className="Polaris-ButtonGroup__Item">
                                                                    <button className="Polaris-Button Polaris-Button--primary"
                                                                    ><span
                                                                        className="Polaris-Button__Content"><span
                                                                            className="Polaris-Button__Text">Done</span></span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="PolarisPortalsContainer"></div>
                                                </div>
                                            </FormLayout>
                                        </form>
                                    </Card>
                                </Layout.Section>
                                <Layout.Section secondary>
                                <Card title="Group will appear on" sectioned>
                                        <form onSubmit={updateProduct}>
                                            <FormLayout>
                                            {/* <p>Add tags to your order.</p> */}
                                            {/* {state.fieldsValues && state.fieldsValues.map((element, index) => ( */}
                                        <div>
                                            <Select
                                               options={options1}
                                               onChange={(event) => setGroup(event)}
                                               value={group || ""}

                                            />
                                        </div>
                                        {/* ))} */}
                                            <div className="Polaris-Card__Footer">
                                            <div className="Polaris-ButtonGroup">
                                                <div className="Polaris-ButtonGroup__Item">
                                                    <button className="Polaris-Button Polaris-Button--primary"
                                                    ><span
                                                        className="Polaris-Button__Content"><span
                                                        className="Polaris-Button__Text">Publish Field Group</span></span>
                                                    </button>
                                                </div>
                                            </div>
                                            </div>
                                            </FormLayout>
                                            </form>
                                        </Card>
                                </Layout.Section>
                            </Layout>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default EditField;