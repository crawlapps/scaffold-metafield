import React, {Component,useState,useEffect} from 'react';
import ReactDOM from 'react-dom';
import {Page, Card, Button,List,Caption,DataTable,Layout,Section} from '@shopify/polaris';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {TitleBar} from '@shopify/app-bridge/actions';

function Index(){

    const [state, setState] = useState([]);

        useEffect(() => {
            console.log("componentDidMount");
            getGroup();
            getPosts();
            // axios.get('/groups').then(res => {
            //
            //     console.log("Groups::List ",res.data.data);
            //
            //     if(res.data) {
            //         if (res.data.success) {
            //             setState(res.data.data)
            //         }
            //     }
            // })
        },[]);

        const getGroup = () => {
            axios.get('/groups').then(res => {
                console.log(res.data);
                console.log("Groups::List ",res.data.data);
                if(res.data) {
                    if (res.data.success) {
                        setState(res.data.data)
                    }
                }
            })
        }
        const getPosts = () => {
            axios.get('/posts').then(res => {
                console.log(res.data);
                console.log("Groups::List ",res.data.data);
                if(res.data) {
                    if (res.data.success) {
                        setState(res.data.data)
                    }
                }
            })
        }


        // const deleteGroup = (id) => {
        //     console.log("Delete Group", id);
        //     axios.post('/groups/delete/'+id).then(res => {

        //         console.log("Group Deleted");
        //         console.log("Groups::List ",res.data.data);

        //         if(res.data) {
        //             if (res.data.success) {
        //                 getGroup();
        //             }
        //         }
        //     })
        // }


       const groups = state;

      return(
           <div>
               <div className="Polaris-Layout">
                   <div
                       className="Polaris-Layout__Section Polaris-Layout__Section--oneHalf">
                       <div className="Polaris-Card">
                           <div className="Polaris-Card__Header">
                               <div
                                   className="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                   <div
                                       className="Polaris-Stack__Item Polaris-Stack__Item--fill max-w-100">
                                       <h2 className="Polaris-Heading">Field Groups</h2>
                                   </div>
                                   <div className="Polaris-Stack__Item">
                                       <div className="Polaris-ButtonGroup">
                                           <div
                                               className="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                                               <Link className='Polaris-Button' to='/groups/add-new'>
                                                   Add new
                                               </Link>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div className="Polaris-Card__Section">
                               <div className="Polaris-ResourceList__ResourceListWrapper">
                                   <table className="Polaris-DataTable__Table">
                                       <tbody>
                                       {groups.length > 0  ?  groups.map(group => (
                                           <tr key={group.id} className="Polaris-DataTable__TableRow Polaris-DataTable--hoverable">
                                               <th className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn max-w-4-5"
                                                   scope="row">
                                                   {group.name}
                                               </th>

                                               <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                                                   <Link
                                                       className='Polaris-Button'
                                                       style={{float: "left"}}
                                                       to={`/groups/edit/${group.id}`}
                                                       key={group.id}
                                                   >
                                                    Edit
                                                   </Link>
                                               </td>
                                               {/* <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                                                   <button  className='Polaris-Button' onClick={() => deleteGroup(group.id)}>Delete</button>
                                               </td> */}
                                           </tr>
                                       ))  :
                                          <tr>
                                              <td colSpan="3">Group not found!</td>
                                          </tr>
                                       }
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
      );
}

export default Index
