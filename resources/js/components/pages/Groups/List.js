import React, { Component, useState, useEffect, EditField } from 'react';
import ReactDOM from 'react-dom';
import { Page, Card, Button, List, Caption, DataTable, Layout, Section } from '@shopify/polaris';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { TitleBar } from '@shopify/app-bridge/actions';

function Index(props) {
    // console.log()
    const [state, setState] = useState([]);

    useEffect(() => {
        console.log("componentDidMount");
        getGroup();
    }, []);

    const getGroup = () => {
        axios.get('/groups').then(res => {
            console.log(res.data);
            console.log("Groups::List ", res.data.data);
            if (res.data) {
                if (res.data.success) {
                    setState(res.data.data)
                }
            }
        })
    }

    // const deleteGroup = (id) => {
    //     console.log("Delete Group", id);
    //     axios.post('/groups/delete/'+id).then(res => {

    //         console.log("Group Deleted");
    //         console.log("Groups::List ",res.data.data);

    //         if(res.data) {
    //             if (res.data.success) {
    //                 getGroup();
    //             }
    //         }
    //     })
    // }


    const groups = state;
    return (
        <div>
            <div className="Polaris-Layout">
                <div
                    className="Polaris-Layout__Section Polaris-Layout__Section--oneHalf">
                    <div className="Polaris-Card">

                        <div className="Polaris-Card__Section">
                            <div className="Polaris-ResourceList__ResourceListWrapper">
                                <table className="Polaris-DataTable__Table">
                                    <thead>
                                        <tr>
                                            <th data-polaris-header-cell="true" className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header">Field Label</th>
                                            <th data-polaris-header-cell="true" className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Field Name</th>
                                            <th data-polaris-header-cell="true" className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col">Field Type</th>
                                            <th data-polaris-header-cell="true" className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric" scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {groups.length > 0 ? groups.map(group => (
                                            <tr className="Polaris-DataTable__TableRow Polaris-DataTable--hoverable">
                                                <th className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn">{group.field_group_items[0]['value']}</th>
                                                <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">{group.field_group_items[0]['value']}</td>
                                                <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric"> {group.field_group_items[0]['value']}</td>
                                                <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric"> <Link
                                                    className='Polaris-Button'
                                                    onClick={props.changeShowResult}
                                                    style={{ float: "left" }}
                                                    to={`/groups/edit/${group.id}`}
                                                    key={group.id}
                                                >
                                                    Edit
                                                </Link></td>
                                            </tr>
                                        )) :
                                            <tr>
                                                <td colSpan="3">Group not found!</td>
                                            </tr>
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Index
