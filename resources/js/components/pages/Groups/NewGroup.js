import axios from 'axios';
import React, { Component,useState,useEffect } from 'react';
import ReactDOM from "react-dom";
import {Link,useNavigate} from "react-router-dom";
import {Page, Card, Button,Caption,DataTable,Layout,Section,FormLayout,TextField,Select,InlineError} from '@shopify/polaris';

function NewGroup(){

    const navigate = useNavigate();

    const data = {
        name : '',
        fieldsValues: [{ field_label: "", field_name : "", field_type : "" }],
        formErrors : {}
    }

    const [state, setState] = useState(data);
    const [group, setGroup] = useState("page");

    const handleFieldChange = (i,event) => {

        let fieldsValues = state.fieldsValues;

        fieldsValues[i][event.target.name] = event.target.value;
        setState({...state, fieldsValues });
    }

    const handleFieldSelectChange = (i,event) => {
        let fieldsValues = state.fieldsValues;
        fieldsValues[i]['field_type'] = event;
        setState({...state, fieldsValues });
    }

    const handleChange = (event) => {
        
        // setState({
        //     [event.target.name] : event.target.value
        // });

        const {name, value} = event.target;
        setState({...state, [name]: value});

    }

    const addFormFields = () => {
        // setState(({
        //     fieldsValues: [...state.fieldsValues, { field_label: "", field_name : "", field_type : "" }]
        // }))

        setState({...state, fieldsValues: [...state.fieldsValues, { field_label: "", field_name : "", field_type : "" }]});
    }

    const removeFormFields = (i) => {

        let fieldsValues = state.fieldsValues;
        fieldsValues.splice(i, 1);
        setState({...state, fieldsValues });
    }


    const handleCreateNewGroup = (event) => {
        event.preventDefault();

        if (handleFormValidation()) {

            const param = {
                "name": state.name,
                "field_group_items" : state.fieldsValues
            }

            console.log("param",param);

            axios.post('/groups/add-new', param)
                .then(response => {

                    console.log("add-group-res",response);

                    navigate('/');

                    // redirect to the homepage
                })
                .catch(error => {
                    console.log("add-group-error",error);
                })
        }
    }

    const handleFormValidation = () => {

        const { name } = state;
        const { fieldsValues } = state.fieldsValues;


        console.log("name",name);


        let formfieldsErrors_msg = {};

        let formErrors = {};
        var formIsValid = true;

        //Student name
        if (!name) {
            formIsValid = false;
            formErrors["nameError"] = "Name is required.";
        }

        let fields_errors = state.fieldsValues.map((element, key) => {

            let formFieldErrors = {};

            if (!element.field_label) {
                formFieldErrors['field_label'] = "field label is required";
                formIsValid = false;
            }
            else {
                formFieldErrors['field_label'] = "";
            }

            if (!element.field_name) {
                formFieldErrors['field_name'] = "field name is required";
                formIsValid = false;
            }
            else {
                formFieldErrors['field_name'] = "";
            }

            if (!element.field_type) {
                formFieldErrors['field_type'] = "field type is required";
                formIsValid = false;
            }
            else {
                formFieldErrors['field_type'] = "";
            }

            return formFieldErrors;

        });

        formErrors['fieldsErrors'] = fields_errors;

        setState({...state, formErrors: formErrors });

        console.log("formIsValid",formIsValid);

        return formIsValid;

    }

        var fieldsErrors = [];

      console.log("state.formErrors",state.formErrors);

         if(state.formErrors && typeof(state.formErrors.fieldsErrors) !== 'undefined') {
                fieldsErrors = state.formErrors.fieldsErrors;
            }


        var { nameError } = '';

        if(state.formErrors && typeof(state.formErrors.nameError) !== 'undefined') {
            nameError = state.formErrors.nameError;
        }

        //   const { fieldsErrors } = this.state.formErrors.fieldsErrors;

        const options = [
            // {label: 'Field Type', value: ''},
            {label: 'Text', value: 'single_line_text_field'},
            {label: 'Wysiwyg', value: 'multi_line_text_field'},
            {label: 'Image', value: 'single_line_text_field'},
            {label: 'Select', value: 'multi_line_text_field'},
            {label: 'Test', value: 'single_line_text_field'}
        ];
        const options1 = [
            // {label: 'Field Type', value: ''},
            {label: 'Page', value: 'page'},
            {label: 'Products', value: 'products'},
            {label: 'Customers', value: 'customers'},
            {label: 'Blogs', value: 'blogs'},
            {label: 'Orders', value: 'orders'},
            {label: 'Product', value: 'product_reference'},
        ];

        return (
            
            <div>
                <div className="Polaris-Layout">
                    <div
                        className="Polaris-Layout__Section Polaris-Layout__Section--oneHalf">
                        <div className="Polaris-Card">
                            <div className="Polaris-Card__Header">
                                <div
                                    className="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                    <div
                                        className="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                        <h2 className="Polaris-Heading">Add New Field Group</h2>
                                    </div>
                                </div>
                            </div>
                            <div className="Polaris-Card__Section">
                                <Layout>
                                    <Layout.Section>
                                        <Card title="Order details" sectioned>
                                            <form onSubmit={handleCreateNewGroup}>
                                                <FormLayout>
                                                    <div>
                                                        <div className='Polaris-TextField'>
                                                            <input
                                                                id='name'
                                                                type='text'
                                                                className={`Polaris-TextField__Input ${ typeof nameError !== 'undefined' ? 'error' : ''}`}
                                                                name='name'
                                                                value={state.name}
                                                                placeholder="Add Title"
                                                                onChange={handleChange}
                                                            />
                                                        </div>
                                                        {nameError &&
                                                        <InlineError message={nameError} fieldID="myFieldID" />
                                                        }
                                                    </div>
                                                    <div>
                                                        <div className="Polaris-Card full-border">
                                                            <div className="Polaris-Card__Header">
                                                                <h2 className="Polaris-Heading">Adding field</h2>
                                                            </div>
                                                            <div className="Polaris-Card__Section">

                                                                <div className="Polaris-Card">
                                                                    <div className="Polaris-Card__Section">

                                                                        {state.fieldsValues && state.fieldsValues.map((element, index) => (

                                                                            <div className="Polaris-Card" key={index} >
                                                                                <div className="Polaris-Card__Section">
                                                                                    <div>
                                                                                        <div className='Polaris-TextField'>
                                                                                            <input type="text"
                                                                                                   name="field_label"
                                                                                                   value={element.field_label || ""}
                                                                                                   className="Polaris-TextField__Input"
                                                                                                   placeholder="Field Label"
                                                                                                   onChange={(event) => handleFieldChange(index, event)}
                                                                                            />
                                                                                        </div>

                                                                                        {typeof fieldsErrors !== 'undefined' && fieldsErrors.length > 0 && ((fieldsErrors.length - 1) >= index) && (
                                                                                            <InlineError message={fieldsErrors[index].field_label} fieldID={index} />
                                                                                        )}
                                                                                    </div>


                                                                                    <div>
                                                                                        <div className='Polaris-TextField'>
                                                                                            <input type="text"
                                                                                                   name="field_name"
                                                                                                   className="Polaris-TextField__Input"
                                                                                                   value={element.field_name || ""}
                                                                                                   placeholder="Field Name"
                                                                                                   onChange={(event) => handleFieldChange(index, event)}
                                                                                            />
                                                                                        </div>
                                                                                        {typeof fieldsErrors !== 'undefined' && fieldsErrors.length > 0 && ((fieldsErrors.length - 1) >= index) && (
                                                                                            <InlineError message={fieldsErrors[index].field_name} fieldID={index} />
                                                                                        )}

                                                                                    </div>

                                                                                    <div>
                                                                                        <Select
                                                                                            placeholder="Field Type"
                                                                                            name="field_type"
                                                                                            options={options}
                                                                                            onChange={(event) => handleFieldSelectChange(index, event)}
                                                                                            value={element.field_type || ""}
                                                                                        />

                                                                                        {typeof fieldsErrors !== 'undefined' && fieldsErrors.length > 0 && ((fieldsErrors.length - 1) >= index) && (
                                                                                            <InlineError message={fieldsErrors[index].field_type} fieldID={index} />
                                                                                        )}

                                                                                    </div>
                                                                                </div>
                                                                                {
                                                                                    index ?
                                                                                        <div
                                                                                            className="Polaris-Card__Footer">
                                                                                            <button onClick={() => removeFormFields(index)}
                                                                                                    className="Polaris-Button Polaris-Button--destructive"
                                                                                                    type="button"><span
                                                                                                className="Polaris-Button__Content"><span
                                                                                                className="Polaris-Button__Text">Remove</span></span>
                                                                                            </button>
                                                                                        </div>
                                                                                        : null
                                                                                }
                                                                            </div>
                                                                        ))}

                                                                    </div>
                                                                    <div className="Polaris-Card__Footer">
                                                                        <div className="Polaris-ButtonGroup">
                                                                            <div className="Polaris-ButtonGroup__Item">
                                                                                <button className="Polaris-Button"
                                                                                        type="button"><span
                                                                                    className="Polaris-Button__Content"><span
                                                                                    className="Polaris-Button__Text"
                                                                                    onClick={() => addFormFields()}
                                                                                >Add Field</span></span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="Polaris-Card__Footer">
                                                                <div className="Polaris-ButtonGroup">
                                                                    <div className="Polaris-ButtonGroup__Item">
                                                                        <button className="Polaris-Button Polaris-Button--primary"
                                                                        ><span
                                                                            className="Polaris-Button__Content"><span
                                                                            className="Polaris-Button__Text">Done</span></span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="PolarisPortalsContainer"></div>
                                                    </div>
                                                </FormLayout>
                                            </form>

                                        </Card>
                                    </Layout.Section>
                                    <Layout.Section secondary>
                                        <Card title="Group will appear on" sectioned>
                                        <form onSubmit={handleCreateNewGroup}>
                                                <FormLayout>
                                            {/* <p>Add tags to your order.</p> */}
                                            {/* {state.fieldsValues && state.fieldsValues.map((element, index) => ( */}

                                            <div>
                                                <Select
                                                    options={options1}
                                                    onChange={(event) => setGroup(event)}
                                                    value={group || ""}

                                                />
                                            </div>
                                            {/* ))} */}
                                             <div className="Polaris-Card__Footer">
                                                <div className="Polaris-ButtonGroup">
                                                    <div className="Polaris-ButtonGroup__Item">
                                                        <button className="Polaris-Button Polaris-Button--primary"
                                                        ><span
                                                            className="Polaris-Button__Content"><span
                                                            className="Polaris-Button__Text">Publish Field Group</span></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                </div>
                                                </FormLayout>
                                                </form>
                                        </Card>
                                    </Layout.Section>
                                </Layout>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

}

export default NewGroup;

