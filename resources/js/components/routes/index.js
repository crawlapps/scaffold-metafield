import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Dashboard from "../pages/Dashboard";

//TEST MODE :: START  //Demo purpose
import React from "react";
import Header from "../layouts/menu";
//TEST MODE :: END


//Groups
import Groups from '../pages/Groups/Index';
import NewGroup from '../pages/Groups/NewGroup';
import EditGroup from '../pages/Groups/EditGroup';
import Post from '../pages/posts/CKEditor';

const WebRoutes = () => {

    return(
        <BrowserRouter>
            <div>
                <Header />
            </div>
            <Routes>
                <Route exact path='/dashboard' element={<Dashboard/>} />
                <Route exact path='/' element={<Groups/>} />
                <Route exact path='/posts' element={<Post/>} />
                <Route exact path='/groups/add-new' element={<NewGroup/>} />
                <Route exact path='/groups/edit/:id' element={<EditGroup/>} />
           </Routes>
        </BrowserRouter>
    )


};

export default WebRoutes;
