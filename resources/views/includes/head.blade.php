<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{env('APP_NAME')}}</title>


<!-- Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">

<link
    rel="stylesheet"
    href="https://unpkg.com/@shopify/polaris@7.4.1/build/esm/styles.css"
/>
