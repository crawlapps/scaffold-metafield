<!DOCTYPE html>
<html lang="{{str_replace('_','-',app()->getLocale())}}">

<head>
     @include('includes.head')

     @if(config('shopify-app.appbridge_enabled'))
         @include('shopify.app-bridge')
     @endif

</head>

<body>

   <div class="container-fluid">
       <div>
           @yield('content')
       </div>
   </div>

    @include('includes.footer-script')

</body>

</html>
