<script src="https://unpkg.com/@shopify/app-bridge@2.0.5"></script>
<script>

    var AppBridge = window['app-bridge'];
    var createApp = AppBridge.default;

    window.shopify_app_bridge = {
        apiKey: '{{ config('shopify-app.api_key') }}',
        shopOrigin: '{{ Auth::user()->name }}',
        host: '{{env('APP_URL')}}',
        forceRedirect: true,
    };
</script>




