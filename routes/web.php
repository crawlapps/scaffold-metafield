<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\FieldGroupController;
use App\Http\Controllers\FieldGroupItemsController;
use App\Http\Controllers\PostsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//})->middleware(['verify.shopify'])->name('home');


Route::get("/login",function(){
    return view("auth.login");
})->name('login');


Route::group(["middleware"=>['verify.shopify']], function () {

   Route::get("/",[AppController::class,'dashboard'])->name('home');
   Route::get("posts",[PostsController::class,'index'])->name('posts');   
});

Route::get("groups",[FieldGroupController::class,'index']);
Route::POST("groups/add-new",[FieldGroupController::class,'store']);
Route::get("groups/edit/{id}",[FieldGroupController::class,'edit']);
Route::POST("groups/update/{id}",[FieldGroupController::class,'update']);
Route::POST("groups/delete/{id}",[FieldGroupController::class,'destroy']);

Route::get("items",[FieldGroupItemsController::class,'index']);

Route::prefix('app')->group(function() {
       Route::get("/field-type-option",[AppController::class,'fieldTypeOption']);
});

//For Testing Purpose.
Route::prefix('test')->group(function() {
    Route::get("/products", [TestController::class, 'productList']);
});


Route::post('posts/likes', [PostsController::class,'updateLikeCounter']);


// Route::resource('posts', 'PostsController');

// Route::post('posts/likes', [PostsController::class, 'index']);

// Route::get('/posts', [PostsController::class, 'index']);